-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-06-2014 a las 06:57:08
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `b`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `rut` int(11) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` int(11) NOT NULL,
  `celular` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `nombre`, `apellido`, `rut`, `direccion`, `telefono`, `celular`, `email`) VALUES
(1, 'mariana', 'valenzuela', 123123123, 'duoc # 18181', 1234567, 12345678, 'hola@hola.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrato`
--

CREATE TABLE IF NOT EXISTS `contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_evento` date NOT NULL,
  `costo` int(11) NOT NULL,
  `tipo_paquete` varchar(1000) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_empleado` int(11) NOT NULL,
  `id_cuenta` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `contrato`
--

INSERT INTO `contrato` (`id`, `fecha_evento`, `costo`, `tipo_paquete`, `id_cliente`, `id_empleado`, `id_cuenta`) VALUES
(1, '2014-06-10', 1000000, 'paquete 1', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cristaleria`
--

CREATE TABLE IF NOT EXISTS `cristaleria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descrip` varchar(1000) NOT NULL,
  `imagen` varchar(30) NOT NULL,
  `stock` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cristaleria`
--

INSERT INTO `cristaleria` (`id`, `descrip`, `imagen`, `stock`) VALUES
(1, 'cristal:\r\nEstas copas son de cristal', './img/cristal.jpg', 100),
(2, 'vidrio:\r\nEstas copas son de vidrio', './img/vidrio.jpg', 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cubiertos`
--

CREATE TABLE IF NOT EXISTS `cubiertos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descrip` varchar(1000) NOT NULL,
  `imagen` varchar(30) NOT NULL,
  `stock` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cubiertos`
--

INSERT INTO `cubiertos` (`id`, `descrip`, `imagen`, `stock`) VALUES
(1, 'acero inoxidable', './img/vajilla.jpg', 100),
(2, 'madera desechable', './img/madera.jpg', 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta_cliente`
--

CREATE TABLE IF NOT EXISTS `cuenta_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rut` int(11) NOT NULL,
  `clave` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cuenta_cliente`
--

INSERT INTO `cuenta_cliente` (`id`, `rut`, `clave`) VALUES
(1, 123123123, '123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE IF NOT EXISTS `empleado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `nom_usu` varchar(30) NOT NULL,
  `clave` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`id`, `nombre`, `apellido`, `nom_usu`, `clave`) VALUES
(1, 'empleado', '1', 'em', '123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `loza`
--

CREATE TABLE IF NOT EXISTS `loza` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descrip` varchar(1000) NOT NULL,
  `imagen` varchar(30) NOT NULL,
  `stock` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `loza`
--

INSERT INTO `loza` (`id`, `descrip`, `imagen`, `stock`) VALUES
(1, 'loza redonda', './img/118-1834_IMG.jpg', 100),
(2, 'loza cuadrada', './img/cuadrada', 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manteleria`
--

CREATE TABLE IF NOT EXISTS `manteleria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descrip` varchar(1000) NOT NULL,
  `imagen` varchar(30) NOT NULL,
  `stock` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `manteleria`
--

INSERT INTO `manteleria` (`id`, `descrip`, `imagen`, `stock`) VALUES
(1, 'mantel deshilado amarillo', './img/mantel_deshilado_con_ama', 100),
(2, 'mantel dorado', './img/MANTEL-DORADO.jpg', 100),
(3, 'mantel fuscia con naranja', './img/mantel_fuscia_con_naranj', 100),
(4, 'mantel organza morado', './img/ORGANZA-MORADAd.jpg', 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE IF NOT EXISTS `pedidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_pedido` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plato_menu`
--

CREATE TABLE IF NOT EXISTS `plato_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_plato` varchar(30) NOT NULL,
  `tipo_plato` varchar(30) NOT NULL,
  `descrip` varchar(1000) NOT NULL,
  `imagen` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `plato_menu`
--

INSERT INTO `plato_menu` (`id`, `nom_plato`, `tipo_plato`, `descrip`, `imagen`) VALUES
(1, 'Chantilly con Fresas o Hawaian', 'postre', 'Torta de vainilla, con doble relleno de crema Chantilly con trozos de melocotón. Decorado con Chantilly y fresas o frutas tropicales.', './img/hawaiano.jpg'),
(2, 'Flan', 'Postre', 'Delicioso flan cubano de queso, con baño de Caramelo.', './img/FLAN.jpg'),
(3, 'Salmón a la Parrilla', 'plato fuerte', 'Filete de salmón fresco cocido a la parrilla, servido con una sala de mantequilla con cilantro y limón y cebolla acaramelada.', './img/salmon.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessiones`
--

CREATE TABLE IF NOT EXISTS `sessiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `browser` varchar(120) NOT NULL,
  `key_scan` varchar(200) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `time_create` datetime NOT NULL,
  `end_session` datetime NOT NULL,
  `key_inter` varchar(200) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_empleado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
