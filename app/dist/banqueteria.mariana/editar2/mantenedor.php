<?php 

/**
 * Contiene los mentodos que se usaran para controlar la informacion de la base de datos.
 *
 * <code>
 * require_once 'mantenedor.php';
 *
 * mantenedor::openDB();
 *
 * mantenedor::query("select '...'");
 *
 * mantenedor::closeDB();
 * </code>
 *
 * @package default
 * @author Jonathan Delgado.
 **/
class mantenedor 
{
	/**
	 * Contiene las creadenciales para la base de datos
	 *
	 * @var array
	 **/
	public static $access_db = array(
		"user"   => "banqueteria",
		"pass"   => "seg12",
		"host"   => "localhost",
		"dbname" => "banqueteria"
	);

	/**
	 * Contiene la coneccion a la base de datos
	 *
	 * @var mysqli
	 **/
	private static $con;

	/**
	 * Abra la base de datos
	 */
	public static function openDB() {
		self::$con = new mysqli(
			self::$access_db["host"],
			self::$access_db["user"],
			self::$access_db["pass"],
			self::$access_db["dbname"]
		);

		if (self::$con->connect_error)
			return false;
		else
			return true;
	}

	/**
	 * Cierra la base de datos
	 */
	public static function closeDB() {
		self::$con->close();
	}

	/**
	 * Realiza la consula a la base de datos
	 *
	 * @return mysqli_result Contiene la respuesta.
	 * @param string $query Contiene la sentencia SQL que ejecutara la base de datos
	 */
	public static function query($query) {
		$memoQuey = self::$con->query($query);


		return $memoQuey;
	}

	/**
	 * Permite realizar una sentencia insert y retornar true o false si se ejecuto correctamente.
	 *
	 * @param string $query Contiene la sentencia sql que es enviada a la base de datos.
	 * @return boolean Si es true se ejecuto correctamente, si es falso no se ejecuto.
	 */
	public static function insert($query) {
		$query = str_replace('"', '', $query);
		return (self::query($query) === TRUE);
	}

	/**
	 * Verifica la existencia de una cuenta empleado
	 *
	 * @return boolean
	 */
	public static function verificaCuentaEmpleado($rut,$pass) {
		$account = self::getRows("empleado","NOM_USU = '$rut' AND CLAVE_USU = '$pass'");

		return (count($account)>0);
	}

	/**
	 * Verifica la existencia de una cuenta cliente
	 *
	 * @return boolean
	 */
	public static function verificaCuentaCliente($rut,$pass) {
		$account = self::getRows("cliente","RUT = '$rut' AND CLAVE = '$pass'");

		return (count($account)>0);
	}

	/**
	 * Crea un nuevo cliente en la base de datos
	 *
	 * @return boolean
	 * @param string $rut Identifica el rut del nuevo cliente
	 * @param string $email Identifica el email del nuevo cliente
	 * @param string $name Contiene el nombre del usuario
	 **/
	public static function nuevoCliente(
		$rut,
		$nombre,
		$email,
		$apellido = null,
		$direccion = null,
		$telefono = null,
		$contrasena = null
	) {
		$query = "INSERT INTO `cliente` (
			`NOMBRE`,
			`APELLIDO`,
			`RUT`,
			`DIRECCION`,
			`TELEFONO`,
			`CELULAR`,
			`EMAIL`,
			`CLAVE`) VALUES (
			'$nombre',
			'$apellido',
			'$rut',
			'$direccion',
			'$telefono',
			'',
			'$email',
			'$contrasena');";
		return self::insert($query);
	}

	/**
	 * Genera un consula sencilla retornando toda las filas de una tabla de la base de datos
	 *
	 * @return array lista con valores de la base de datos
	 * @param string $table Nombre de la tabla
	 * @param string $where de la consulta a la base de datos
	 * @param integer $limRow Defini la cantidad de respuestas por consulta
	 * @param integer $startIn Si se define el largo de las consultas defini desde donde empezar a mostrar las filas
	 */
	public static function getRows($table, $where = null, $limRow = 0, $startIn = 0) {
		$limit = "";
		$collRows = array();

		if (!is_null($where)) {
			$where = "where ". $where;
		} else {
			$where = "";
		}

		if ($limRow > 0) {
			$limit = "LIMIT ".$startIn." , ".$limRow;
		}

		$query = "select * from `".$table."` ".$where." ".$limit;

		$result = self::query($query);

		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$collRows[] = $row;
			}
			$result->close();
			self::$con->next_result();
		}


		return $collRows;
	}

	/**
	 * Retorna todos los Empleados
	 * 
	 * @return array Contiene todos los empleados
	 * @param string $condition Condicion para buscar en la base de datos
	 */
	public static function getEmpleados($condition = null) {
		return self::getRows("empleado",$condition);
	}

	/**
	 * Retorna todos los Clientes
	 * 
	 * @return array Contiene todos los empleados
	 * @param string $condition Condición para buscar en la base de datos
	 */
	public static function getCliente($condition = null) {
		return self::getRows("cliente",$condition);
	}

	/**
	 * Retorna todos las cuentas de los Clientes
	 * 
	 * @return array Contiene todos los empleados
	 * @param string $condition Condición para buscar en la base de datos
	 */
	public static function getCuentaCliente($condition = null) {
		return self::getRows("cuenta_cliente",$condition);
	}

	/**
	 * 
	 * @return array Contiene todas las sesiones echas a la fecha
	 * @param string $condition Contiene la condición al momento de buscar.
	 */
	public static function getSessions($condition = null) {
		return self::getRows("sessiones",$condition);
	}

	/**
	 * Sube un nuevo articulo a la vase de datos y dierencia a que tipo pertenece
	 * 
	 * @param string $nombre  define el nombre del neuvo articulo
	 * @param string $tipo   Define el tipo al cual pertenece al articulo
	 * @param integer $cantidad Define la cantaidad de articulos al momento de crearlo
	 * @param string $imagen  Define la ruta de nuestra imagen
	 * @return void
	 */
	public static function nuevoArticulo($nombre,$tipo,$cantidad,$imagen = null) {
		$type = null;

		// defino como texto para sql
		if ($imagen != null || $imagen != '') {
			$imagen = "'$cantidad'";
		} else {
			$imagen = "NULL";
		}

		// remplazo el valor de $type si el tipo definido es encontrado
		switch ($tipo) {
			case 'cristaleria':
				$type = 'cristaleria';
				break;
			case 'cubiertos':
				$type = 'cubiertos';
				break;
			case 'manteleria':
				$type = 'manteleria';
				break;
			case 'loza':
				$type = 'loza';
				break;
		}

		// si no se define un tipo definid retorna false.
		if ($type == null) {
			return false;
		}

		return self::insert("INSERT INTO `$type` (
			`DESCRIP`,
			`IMAGEN`,
			`STOCK`
		) VALUES (
			'$nombre',
			$imagen,
			'$cantidad'
		);");
	}

	/**
	 * Permite eliminar un articulo de la base de datos
	 * 
	 * @param  integer $ID   Define el id de la base de datos del articulo.
	 * @param  string  $tipo Define la categoría a la cual pertenece el
	 *                       articulo. de esa forma puede eliminar
	 *                       correctamente.
	 * @return boolean       Retorna true si se elimino correctamente.
	 */
	public static function removeArticulo($ID,$tipo) {
		$type = null;
		switch ($tipo) {
			case 'cristaleria':
				$type = 'cristaleria';
				break;
			case 'cubiertos':
				$type = 'cubiertos';
				break;
			case 'manteleria':
				$type = 'manteleria';
				break;
			case 'loza':
				$type = 'loza';
				break;
		}

		$query = "DELETE FROM `$type` WHERE `ID` = $ID";
		
		// si no se define un tipo definid retorna false.
		if ($type == null) {
			return false;
		}

		return (self::query($query)===TRUE);
	}

	/**
	 * Actualiza articulo a la base de datos y diferencia a que tipo pertenece
	 *
	 * @param string $ID Define el id del producto
	 * @param string $nombre define el nombre del articulo
	 * @param string $tipo Define el tipo al cual pertenece al articulo
	 * @param integer $cantidad Define la cantidad de artículos al momento de crearlo
	 * @param string $imagen Define la ruta de nuestra imagen
	 * @return void
	 */
	public static function updateArticulo($ID,$nombre,$tipo,$cantidad,$imagen = null) {
		$type = null;

		// defino como texto para sql
		if ($imagen != null || $imagen != '') {
			$imagen = "'$cantidad'";
		} else {
			$imagen = "NULL";
		}

		// remplazo el valor de $type si el tipo definido es encontrado
		switch ($tipo) {
			case 'cristaleria':
				$type = 'cristaleria';
				break;
			case 'cubiertos':
				$type = 'cubiertos';
				break;
			case 'manteleria':
				$type = 'manteleria';
				break;
			case 'loza':
				$type = 'loza';
				break;
		}

		// si no se define un tipo definid retorna false.
		if ($type == null) {
			return false;
		}

		return self::insert("UPDATE `$type` SET
			`DESCRIP` = '$nombre',
			`IMAGEN` = $imagen,
			`STOCK` = '$cantidad'
			WHERE `ID` = $ID;");
	}

	/**
	 * Retorna todos los articulos del inventario interno
	 * 
	 * @return array contiene todos los detalles de los artículos
	 */
	public static function getArticulos($condition=null){
		$ret = array(
			'cristaleria' => array(),
			'cubiertos'  => array(),
			'manteleria' => array(),
			'loza'    => array()
			);
		$ret['cristaleria'] = self::getRows('cristaleria',$condition);
		$ret['cubiertos']  = self::getRows('cubiertos',$condition);
		$ret['manteleria'] = self::getRows('manteleria',$condition);
		$ret['loza']    = self::getRows('loza',$condition);

		return $ret;
	}

} // END class mantenedor 


/**
 * Controla la sesiones de las aplicaciones mediante el reconocimiento de usuario y navegador.
 *
 *
 * @package default
 * @author Jonathan Delgado.
 **/
class session 
{
	/**
	 * Contiene el nombre usado en el navegador de la sesión
	 *
	 * @var string
	 */
	private static $name_cookie = "session";

	/**
	 * Contiene el tiempo en cuanto expira la sesión
	 *
	 * @var string
	 **/
	private static $time_expire = 2592000;

	/**
	 * Genera una nueva sesión en el navegador que es usada por la aplicación.
	 *
	 * @return string Contiene la cadena de la sesión
	 **/
	public static function newKey()
	{
		$date = date("c");
		$nrand = rand(11111,99999);
		$str_key = "T".$nrand."D".$date;

		setcookie(self::$name_cookie,$str_key,time()+self::$time_expire);

		return $str_key;
	}

	/**
	 * Retorna el valor de la clave para encontrar la sesión 
	 * @return string Valor de la cadena sesión
	 */
	public static function getCookie() {
		return @$_COOKIE[self::$name_cookie];
	}

	/**
	 * Crea una sesión para el empleado
	 * 
	 * @param string $rutEmpleado define la clave que usara para buscar al usuario
	 * @return void
	 */
	public static function newSessionEmpleado($rutEmpleado) {
		$ID_EMPLEADO = mantenedor::getEmpleados("NOM_USU = '$rutEmpleado'");
		$ID_EMPLEADO = $ID_EMPLEADO[0];
		$ID_EMPLEADO = $ID_EMPLEADO["ID_EMPLEADO"];
		$key_session = self::newKey();
		$BROWSER = $_SERVER['HTTP_USER_AGENT'];
		$IP = self::GetUserIP();
		mantenedor::query("INSERT INTO `banqueteria`.`sessiones` (
			`BROWSER`	, `KEY_SCAN`	, `IP`	, `ID_CLIENTE`	, `ID_EMPLEADO`	, `TIME_CREATE`	, `END_SESSION`	, `KEY_INTER`) VALUES (
			'$BROWSER'	, '$key_session', '$IP'	, NULL			, $ID_EMPLEADO 	, now() 		, NULL			, '$key_session');");
	}

	/**
	 * Crea una sesión para el empleado
	 * 
	 * @param string $rutCliente define la clave que usara para buscar al usuario
	 * @return void
	 */
	public static function newSessionCliente($rutCliente) {
		$ID_cliente = mantenedor::getCuentaCliente("NOM_USU = '$rutCliente'");
		$ID_cliente = $ID_cliente[0];
		$ID_cliente = $ID_cliente["ID_CUENTA"];
		$key_session = self::newKey();
		$BROWSER = $_SERVER['HTTP_USER_AGENT'];
		$IP = self::GetUserIP();
		mantenedor::query("INSERT INTO `banqueteria`.`sessiones` (
			`BROWSER`	, `KEY_SCAN`	, `IP`	, `ID_CLIENTE`	, `ID_EMPLEADO`	, `TIME_CREATE`	, `END_SESSION`	, `KEY_INTER`) VALUES (
			'$BROWSER'	, '$key_session', '$IP'	, $ID_cliente	, null	 		, now() 		, NULL			, '$key_session');");
	}

	/**
	 * Este metodo permite eliminar la session del navegador.
	 * 
	 * @return boolean Si se ejecuto completamente la session retornara true
	 */
	public static function removeSession() {
		if (isset($_COOKIE[self::$name_cookie])) {
			unset($_COOKIE[self::$name_cookie]);
			setcookie(self::$name_cookie, null, -1, '/');
			return true;
		}
		else return false;
	}

	/**
	 * Comprueba que esta con una cuenta empleado y retorna la cuenta de otro modo retorna false
	 *
	 * @return string contiene id de la cuenta de la sesión iniciada de otro modo retorna valor nulo (null)
	 */
	public static function verificaSessionEmpleado() {
		$respuesta = mantenedor::query("SELECT * FROM `sessiones` WHERE KEY_SCAN = '".self::getCookie()."'");

		if ($respuesta) {
			$id_cliente = null;

			$id_cliente = $respuesta->fetch_assoc();
			$id_cliente = $id_cliente["ID_EMPLEADO"];

			return $id_cliente;
		} else {
			return null;
		}
	}

	/**
	 * Comprueba que esta con una cuenta Cliente y retorna la cuenta de otro modo retorna false
	 *
	 * @return string contiene id de la cuenta de la sesión iniciada de otro modo retorna valor nulo (null)
	 */
	public static function verificaSessionCliente() {
		$respuesta = mantenedor::query("SELECT * FROM `sessiones` WHERE KEY_SCAN = '".self::getCookie()."'");

		if ($respuesta) {
			$id_cliente = null;

			$id_cliente = $respuesta->fetch_assoc();
			$id_cliente = $id_cliente["ID_CLIENTE"];

			return $id_cliente;
		} else {
			return null;
		}
	}

	/**
	 * Obtiene la ip del cliente.
	 * 
	 * @author ???
	 * @url http://thepcspy.com/read/getting_the_real_ip_of_your_users/
	 */
	public static function GetUserIP() {
		if (isset($_SERVER)) {
			if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
				return $_SERVER["HTTP_X_FORWARDED_FOR"];			
			if (isset($_SERVER["HTTP_CLIENT_IP"]))
				return $_SERVER["HTTP_CLIENT_IP"];
			return $_SERVER["REMOTE_ADDR"];
		}
		if (getenv('HTTP_X_FORWARDED_FOR'))
			return getenv('HTTP_X_FORWARDED_FOR');
		if (getenv('HTTP_CLIENT_IP'))
			return getenv('HTTP_CLIENT_IP');
		return getenv('REMOTE_ADDR');
	}
}
