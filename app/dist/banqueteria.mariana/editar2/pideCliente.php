<?php
include ("mantenedor.php");
$dbhost=mantenedor::$access_db["host"];
$dbname=mantenedor::$access_db["dbname"];
$dbuser=mantenedor::$access_db["user"];
$dbpass=mantenedor::$access_db["pass"];

$db = new mysqli($dbhost,$dbuser,$dbpass,$dbname);

if (isset($_POST) && count($_POST)>0)
{
	if ($db->connect_errno) 
	{
		die ("<span class='ko'>Fallo al conectar a MySQL: (" . $db->connect_errno . ") " . $db->connect_error."</span>");
	}
	else
	{
		$query=$db->query("update cliente set ".$_POST["campo"]."='".$_POST["valor"]."' where ID_CLIENTE='".intval($_POST["id"])."' limit 1");
		if ($query) echo "<span class='ok'>Valores modificados correctamente.</span>";
		       else echo "<span class='ko'>".$db->error."</span>";
	}
}

if (isset($_GET) && count($_GET)>0)
{
	if (isset($_GET["newFill"])) {
		if ($db->connect_errno) {
			die ("<span class='ko'>Fallo al conectar a MySQL: (" . $db->connect_errno . ") " . $db->connect_error."</span>");
		} else 
		{
			$query=$db->query("INSERT INTO `cliente` (`ID_CLIENTE`, `NOMBRE`, `APELLIDO`, `RUT`, `DIRECCION`, `TELEFONO`, `CELULAR`, `EMAIL`, `CLAVE`) VALUES (NULL, '', NULL, '', NULL, NULL, NULL, NULL, '');");
			if ($db->insert_id) echo "<span class='ok'>Valores modificados correctamente.</span>";
						   else echo "<span class='ko'>".$db->error."</span>";	
		}
	}

	if ($db->connect_errno) 
	{
		die ("<span class='ko'>Fallo al conectar a MySQL: (" . $db->connect_errno . ") " . $db->connect_error."</span>");
	}
	else
	{
		$query=$db->query("select * from cliente order by ID_CLIENTE asc");
		$datos=array();
		while ($usuarios=$query->fetch_array())
		{
			$datos[]=array(	"id"=>$usuarios["ID_CLIENTE"],
							"nombre"=>$usuarios["NOMBRE"],
							"apellido"=>$usuarios["APELLIDO"],
							"rut"=>$usuarios["RUT"],
							"direccion"=>$usuarios["DIRECCION"],
							"telefono"=>$usuarios["TELEFONO"],
							"celular"=>$usuarios["CELULAR"],
							"email"=>$usuarios["EMAIL"]
			);
		}
		echo json_encode($datos);
	}
}
?>