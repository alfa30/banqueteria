<?php
include ("mantenedor.php");
$dbhost=mantenedor::$access_db["host"];
$dbname=mantenedor::$access_db["dbname"];
$dbuser=mantenedor::$access_db["user"];
$dbpass=mantenedor::$access_db["pass"];
$db = new mysqli($dbhost,$dbuser,$dbpass,$dbname);

if (isset($_POST) && count($_POST)>0)
{
	if ($db->connect_errno) 
	{
		die ("<span class='ko'>Fallo al conectar a MySQL: (" . $db->connect_errno . ") " . $db->connect_error."</span>");
	}
	else
	{
		$query=$db->query("update plato_menu set ".$_POST["campo"]."='".$_POST["valor"]."' where id='".intval($_POST["id"])."' limit 1");
		if ($query) echo "<span class='ok'>Valores modificados correctamente.</span>";
		else echo "<span class='ko'>".$db->error."</span>";
	}
}

if (isset($_GET) && count($_GET)>0)
{
	if ($db->connect_errno) 
	{
		die ("<span class='ko'>Fallo al conectar a MySQL: (" . $db->connect_errno . ") " . $db->connect_error."</span>");
	}
	else
	{
		$query=$db->query("select * from plato_menu order by id asc");
		$datos=array();
		while ($usuarios=$query->fetch_array())
		{
			$datos[]=array(	"id"=>$usuarios["id"],
							"nom_plato"=>$usuarios["nom_plato"],
							"tipo_plato"=>$usuarios["tipo_plato"],
							"descrip"=>$usuarios["descrip"],
							"imagen"=>$usuarios["imagen"]
			);
		}
		echo json_encode($datos);
	}
}
?>