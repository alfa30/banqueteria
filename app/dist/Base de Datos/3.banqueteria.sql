-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-06-2014 a las 22:10:04
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `banqueteria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `ID_CLIENTE` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE` varchar(50) NOT NULL,
  `APELLIDO` varchar(50) NOT NULL,
  `RUT` int(11) NOT NULL,
  `DIRECCION` varchar(70) NOT NULL,
  `TELEFONO` int(11) NOT NULL,
  `CELULAR` int(11) NOT NULL,
  PRIMARY KEY (`ID_CLIENTE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrato`
--

CREATE TABLE IF NOT EXISTS `contrato` (
  `ID_CONTRATO` int(11) NOT NULL AUTO_INCREMENT,
  `COSTO` int(11) NOT NULL,
  `FECHA_EVENTO` date NOT NULL,
  `TIPO_PAQUETE` int(11) NOT NULL,
  `ID_CLIENTE` int(11) NOT NULL,
  `ID_EMPLEADO` int(11) NOT NULL,
  `ID_CUENTA` int(11) NOT NULL,
  PRIMARY KEY (`ID_CONTRATO`,`ID_CLIENTE`,`ID_EMPLEADO`,`ID_CUENTA`),
  KEY `Relationship3` (`ID_CLIENTE`),
  KEY `Relationship4` (`ID_EMPLEADO`),
  KEY `Relationship18` (`ID_CUENTA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cristaleria`
--

CREATE TABLE IF NOT EXISTS `cristaleria` (
  `ID_CRISTALERIA` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRIP_CRISTALERIA` TEXT NOT NULL,
  `IMAGEN_CRISTALERIA` text NOT NULL,
  `STOCK` varchar(20) NOT NULL,
  PRIMARY KEY (`ID_CRISTALERIA`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cubiertos`
--

CREATE TABLE IF NOT EXISTS `cubiertos` (
  `ID_CUBIERTO` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRIP_CUBIERTO` TEXT NOT NULL,
  `IMAGEN_CUBIERTO` blob NOT NULL,
  `STOCK` varchar(20) NOT NULL,
  PRIMARY KEY (`ID_CUBIERTO`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta_cliente`
--

CREATE TABLE IF NOT EXISTS `cuenta_cliente` (
  `ID_CUENTA` int(11) NOT NULL AUTO_INCREMENT,
  `CLAVE` varchar(20) NOT NULL,
  `NOM_USU` varchar(20) NOT NULL,
  PRIMARY KEY (`ID_CUENTA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE IF NOT EXISTS `empleado` (
  `ID_EMPLEADO` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE_EMPLEADO` varchar(50) NOT NULL,
  `APELLIDO_EMPLEADO` varchar(50) NOT NULL,
  `NOM_USU` varchar(30) NOT NULL,
  `CLAVE_USU` varchar(30) NOT NULL,
  PRIMARY KEY (`ID_EMPLEADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE IF NOT EXISTS `inventario` (
  `ID_INVENTARIO` int(11) NOT NULL AUTO_INCREMENT,
  `ID_MENU` int(11) NOT NULL,
  `ID_PLATO` int(11) NOT NULL,
  `ID_CRISTALERIA` int(11) NOT NULL,
  `ID_LOZA` int(11) NOT NULL,
  `ID_CUBIERTO` int(11) NOT NULL,
  `ID_TIPO_SERVICIO` int(11) NOT NULL,
  `ID_MANTELERI` int(11) NOT NULL,
  PRIMARY KEY (`ID_INVENTARIO`),
  KEY `Relationship8` (`ID_MENU`,`ID_PLATO`),
  KEY `Relationship10` (`ID_CRISTALERIA`),
  KEY `Relationship11` (`ID_LOZA`),
  KEY `Relationship12` (`ID_CUBIERTO`),
  KEY `Relationship13` (`ID_TIPO_SERVICIO`),
  KEY `Relationship16` (`ID_MANTELERI`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `loza`
--

CREATE TABLE IF NOT EXISTS `loza` (
  `ID_LOZA` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRIP_LOZA` TEXT NOT NULL,
  `IMAGEN_LOZA` blob NOT NULL,
  `STOCK` varchar(20) NOT NULL,
  PRIMARY KEY (`ID_LOZA`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manteleria`
--

CREATE TABLE IF NOT EXISTS `manteleria` (
  `ID_MANTELERIA` int(11) NOT NULL AUTO_INCREMENT,
  `DESCIP_MANTELERIA` TEXT NOT NULL,
  `IMAGEN_MANTELERIA` blob NOT NULL,
  `STOCK` varchar(20) NOT NULL,
  PRIMARY KEY (`ID_MANTELERIA`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `ID_MENU` int(11) NOT NULL AUTO_INCREMENT,
  `DECRIP_MENU` TEXT NOT NULL,
  `ID_PLATO` int(11) NOT NULL,
  PRIMARY KEY (`ID_MENU`,`ID_PLATO`),
  KEY `Relationship7` (`ID_PLATO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paquete`
--

CREATE TABLE IF NOT EXISTS `paquete` (
  `ID_PAQUETE` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRIP_PAQUETE` TEXT NULL,
  PRIMARY KEY (`ID_PAQUETE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE IF NOT EXISTS `pedidos` (
  `ID_PEDIDO` int(11) NOT NULL AUTO_INCREMENT,
  `FECHA_PEDIDPO` date NOT NULL,
  `ID_CLIENTE` int(11) NOT NULL,
  `ID_INVENTARIO` int(11) NOT NULL,
  PRIMARY KEY (`ID_PEDIDO`,`ID_CLIENTE`,`ID_INVENTARIO`),
  KEY `Relationship1` (`ID_CLIENTE`),
  KEY `Relationship2` (`ID_INVENTARIO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plato_menu`
--

CREATE TABLE IF NOT EXISTS `plato_menu` (
  `ID_PLATO` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRIP_PLATO` TEXT NOT NULL,
  `IMAGEN_PLATO` blob NOT NULL,
  PRIMARY KEY (`ID_PLATO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro`
--

CREATE TABLE IF NOT EXISTS `registro` (
  `ID_REGISTRO` int(11) NOT NULL AUTO_INCREMENT,
  `FECHA_PEDIDO` date NOT NULL,
  `ID_PEDIDO` int(11) NOT NULL,
  `ID_CLIENTE` int(11) NOT NULL,
  `ID_INVENTARIO` int(11) NOT NULL,
  `ID_EMPLEADO` int(11) NOT NULL,
  PRIMARY KEY (`ID_REGISTRO`,`ID_PEDIDO`,`ID_CLIENTE`,`ID_INVENTARIO`,`ID_EMPLEADO`),
  KEY `Relationship5` (`ID_PEDIDO`,`ID_CLIENTE`,`ID_INVENTARIO`),
  KEY `Relationship6` (`ID_EMPLEADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_servicio_de_mesa`
--

CREATE TABLE IF NOT EXISTS `tipo_servicio_de_mesa` (
  `ID_TIPO_SERVICIO` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_SERVICIO` varchar(50) NOT NULL,
  `DESCRIP_SERVICIO` TEXT NOT NULL,
  `STOCK` varchar(20) NOT NULL,
  PRIMARY KEY (`ID_TIPO_SERVICIO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;




-- --
-- -- Restricciones para tablas volcadas
-- --

-- --
-- -- Filtros para la tabla `contrato`
-- --
-- ALTER TABLE `contrato`
--   ADD CONSTRAINT `Relationship18` FOREIGN KEY (`ID_CUENTA`) REFERENCES `cuenta_cliente` (`ID_CUENTA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
--   ADD CONSTRAINT `Relationship3` FOREIGN KEY (`ID_CLIENTE`) REFERENCES `cliente` (`ID_CLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
--   ADD CONSTRAINT `Relationship4` FOREIGN KEY (`ID_EMPLEADO`) REFERENCES `empleado` (`ID_EMPLEADO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- --
-- -- Filtros para la tabla `inventario`
-- --
-- ALTER TABLE `inventario`
--   ADD CONSTRAINT `Relationship10` FOREIGN KEY (`ID_CRISTALERIA`) REFERENCES `cristaleria` (`ID_CRISTALERIA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
--   ADD CONSTRAINT `Relationship11` FOREIGN KEY (`ID_LOZA`) REFERENCES `loza` (`ID_LOZA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
--   ADD CONSTRAINT `Relationship12` FOREIGN KEY (`ID_CUBIERTO`) REFERENCES `cubiertos` (`ID_CUBIERTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
--   ADD CONSTRAINT `Relationship13` FOREIGN KEY (`ID_TIPO_SERVICIO`) REFERENCES `tipo_servicio_de_mesa` (`ID_TIPO_SERVICIO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
--   ADD CONSTRAINT `Relationship16` FOREIGN KEY (`ID_MANTELERI`) REFERENCES `manteleria` (`ID_MANTELERIA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
--   ADD CONSTRAINT `Relationship8` FOREIGN KEY (`ID_MENU`, `ID_PLATO`) REFERENCES `menu` (`ID_MENU`, `ID_PLATO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- --
-- -- Filtros para la tabla `menu`
-- --
-- ALTER TABLE `menu`
--   ADD CONSTRAINT `Relationship7` FOREIGN KEY (`ID_PLATO`) REFERENCES `plato_menu` (`ID_PLATO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- --
-- -- Filtros para la tabla `pedidos`
-- --
-- ALTER TABLE `pedidos`
--   ADD CONSTRAINT `Relationship1` FOREIGN KEY (`ID_CLIENTE`) REFERENCES `cliente` (`ID_CLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
--   ADD CONSTRAINT `Relationship2` FOREIGN KEY (`ID_INVENTARIO`) REFERENCES `inventario` (`ID_INVENTARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- --
-- -- Filtros para la tabla `registro`
-- --
-- ALTER TABLE `registro`
--   ADD CONSTRAINT `Relationship5` FOREIGN KEY (`ID_PEDIDO`, `ID_CLIENTE`, `ID_INVENTARIO`) REFERENCES `pedidos` (`ID_PEDIDO`, `ID_CLIENTE`, `ID_INVENTARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
--   ADD CONSTRAINT `Relationship6` FOREIGN KEY (`ID_EMPLEADO`) REFERENCES `empleado` (`ID_EMPLEADO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
-- /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
-- /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

