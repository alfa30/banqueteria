/**
* desarrollado para controlar los componentes de carga y alertas.
*/
(function(j,w){

	/* Crea el modelo para el objeto cargando */
	w.loading = function(objeto) {
		var loading = j(objeto);
		this.nprocess = 0;
		this.delayTime = 0;
		this.on = function () {
			this.nprocess = this.nprocess + 1;
			loading.show(this.delayTime);
		}
		this.off = function () {
			if (this.nprocess>=0) {
				this.nprocess = this.nprocess - 1;
			}
			if (this.nprocess <= 0) {	
				loading.hide(this.delayTime);
			}
		}
		this.end = function() {
			this.nprocess = 0;
			loading.hide(this.delayTime);
		}
		this.end();
	}

	/* Crea modelo para objeto alerta */
	var _alert = function(argument) {
		// body...
	}
	var _alerts = function(objeto) {
		
	}

	w.alerts = function(objeto) {
		this.timeView = 5000;
		this.nameEffect = "bounceOut";
		this.timeEffect = 1000;
		this.removeNameEffect = "animated";
		var box_alerts = $(objeto);
		var alert_model = null;
		/* Detectar modelo */
		if (box_alerts.find(".alert").length >= 1) {
			alert_model = $(box_alerts.find(".alert")[0]);
			this.nameEffect = alert_model.attr("out-class");
			this.removeNameEffect = alert_model.attr("remove-out-class");
			this.timeEffect = alert_model.css("animation-duration");
			box_alerts.find(".alert")[0].remove();

			if (this.timeEffect) {
				var n = parseFloat(this.timeEffect);
				if ((n+"s") == this.timeEffect) {
					this.timeEffect = n * 1000;
				};
			};

		}
		this.new = function(mensaje,type,llong) {
			if (typeof type == "number") {
				llong = type;
				type = null;
			};

			if (llong == Infinity) {
				llong = -1;
			};

			var esn = function(alert_model,box_alerts,nameEffect,timeView,timeEffect,removeNameEffect,mensaje,type,llong) {
				if (!type) {
					type="warning"
				};
				var alert = $(alert_model.clone());
				alert.append(mensaje);
				alert.addClass("alert-"+type);
				box_alerts.append(alert);
				if (llong != -1) {
					setTimeout(function() {
						alert.removeClass(removeNameEffect);
						alert.addClass(nameEffect);
						setTimeout(function() {
							alert.remove();
						}, timeEffect);
					}, (llong != null && llong != -1)?llong:timeView);
				}
			}
			new esn(alert_model,box_alerts,this.nameEffect,this.timeView,this.timeEffect,this.removeNameEffect,mensaje,type,llong);
		}
	}

})(jQuery,window);
