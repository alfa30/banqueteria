/**
 * Permite generar eventos según navegación por hash.
 *
 * @author Jonathan Delgado Zamorano <jonad.correo@gmail.com>
 */
(function(jq,win){

	var fn = function(hash,fn) {
		var evjec = function () {
			// recupera el hash del navegador
			var tab = window.location.hash;
			// si el hash ingresado es igual al del navegado
			if (tab == hash || tab == "#"+hash) {
				fn(tab);
			};
		}

		jq(window).bind('hashchange', evjec);
		jq(window).load(evjec);
	}

	win.hash = fn;

})(jQuery,window)