<?php

header('Content-Type: application/json');

$archivosSubidos = array("files"=>array(),"error"=>array());

/**
 * Configuración
 */
$ruta="archivos/";

  //Como no sabemos cuantos archivos van a llegar, iteramos la variable $_FILES
  foreach ($_FILES as $key) {
    if($key['error'] == UPLOAD_ERR_OK ){//Verificamos si se subio correctamente
      $nombre = $key['name'];//Obtenemos el nombre del archivo
      $temporal = $key['tmp_name']; //Obtenemos el nombre del archivo temporal
      $tamano= ($key['size'] / 1000)."Kb"; //Obtenemos el tamaño en KB

      //////////////////
      // Nuevo nombre //
      //////////////////
      $nombre = md5(rand(111111,999999).date("c")).".".end(explode(".", $nombre));

      copy($temporal, $ruta . $nombre); //Movemos el archivo temporal a la ruta especificada
      //El echo es para que lo reciba jquery y lo ponga en el div "cargados"
      //echo "$nombre - $tamano";
      $archivosSubidos["files"][] = dirname($_SERVER['REQUEST_URI']) . "/" . $ruta . $nombre;
    }else{
      $archivosSubidos["error"][] = $key['error'];
    }
  }

  echo json_encode($archivosSubidos);
?>