/*

Entrada para control de cliente

 */
hash("users_grid",function(){

/**
 * Parámetros de configuración
 */
var pipeControlServer = "pipeClientes";
var pipeControlLocal  = "pipe-cliente";
    

    request({
        pipeControlServer : "pipeClientes"
    },function(json){
        json = json["pipeClientes"];

        console.log(json["pipeClientes"]);

        // condiciona la suplicacionero del formulario de edición
        // true: el td se convierte en formulario
        // false: el td no se convierte
        controldeediciones = true;
        // almacena temporalmente el td original
        // en cuanto se cancele utilizara este objeto para restaurarlo
        var origin;

        $('[cap="pipe-cliente"]')
        .find('tbody')
        .html(
            createTbodyIntoArray(json).html()
        )
        // Busca los campos edita-bles
        .find('.editable')
        // Este evento se dispara en cuanto se hace clic sobre el
        // objeto TD
        .click(function(event) {
            event.preventDefault();

            if (controldeediciones == true) {
                var td, campo, valor, id;

                origin   = $(this).clone();
                imgclose = $(this).data('imgclose');
                td       = $(this).closest("td");
                campo    = $(this).closest("td").data("campo");
                valor    = $(this).text();
                id       = $(this).closest("tr").find(".id").text();

                controldeediciones = false;

                ///////////////////////////////////////
                // objeto input para modificar datos //
                ///////////////////////////////////////
                var inputeditcampo =
                $("<input>")
                .addClass('form-control')
                .attr({
                    "type": 'text',
                    "name": campo
                })
                .val(valor)

                // si detacta que es una objeto tipo rut
                // crea regla auto formato rut
                if (campo == "rut" || campo == "RUT")
                    $(inputeditcampo).rut({formatOn: 'keyup', validateOn: 'keyup'});

                td
                .text("")
                .append(inputeditcampo)
                .append(
                    $("<a>")
                    .addClass('btn btn-primary btn-block guardar')
                    .append(
                        $("<i>")
                        .addClass('glyphicon glyphicon-floppy-disk')
                    )
                    .append("Guardar")
                    .click(function(event) {
                        event.preventDefault();

                        load.on();

                        // Captura el valor del input
                        nuevovalor = $(this).closest("td").find("input").val();

                        if(nuevovalor.trim()!="")
                        {
                            request({
                                method: "POST"
                            },{
                                pipeControlServer     : "pipeClientes",
                                campo                 : campo,
                                valor                 : nuevovalor,
                                id                    : id
                            },function( msg ) {
                                load.off();
                                ale.new( msg );
                                td.html(
                                    $("<span>")
                                        .text(nuevovalor)
                                );
                                //setTimeout(function() {$('.ok,.ko').fadeOut('fast');}, 3000);
                                setTimeout(function(){
                                    controldeediciones = true;
                                }, 0);
                            });
                        } 
                        else {
                            load.off();
                            ale.new("<p class='ko'>Debes ingresar un valor</p>");
                        }
                    })
                )
                .append(
                    $("<a>")
                    .addClass('btn btn-default btn-block cancelar')
                    // agregar icono remove
                    .append(
                        $("<i>")
                        .addClass('glyphicon glyphicon-remove')
                    )
                    // agregar texto Cancelar
                    .append('Cancelar')
                    /*
                    Se activa al precionar cancelar
                    restaura el TD y modifica el control de deciciones a false
                     */
                    .click(function(event) {
                        $(this)
                            .closest('td')
                            .html(origin.html())
                            ;
                        setTimeout(function(){
                            controldeediciones = true;
                        }, 0);
                    })
                )
            }
        });
    });

});