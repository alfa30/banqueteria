// Hoja de controles pipe //

/**
 * Este metodo es creado para crear una etiqueta tbody basada en un arreglo entregado con multiples parametros
 *
 * @param {array} array Contiene el arreglo de todos los objetos que iran en el tr
 * @return {jquery Object} Contiene al objeto Tbody
 */
function createTbodyIntoArray (array) {
    ret = $("<tbody>");

    for (var i = array.length - 1; i >= 0; i--) {
        ret.append(generaTableConentTr(array[i]));
    };

    return ret;
}

/**
 * Esta funcion es creada especificamente para la creacion de objetos TR que posee la aplicacion pipe para controlar sus tablas. 
 *      
 * @param  {Object} obj Contiene todos los atributos de la tabla
 *                      ej. {
 *                          id: 1,
 *                          Nombre: "Juan",
 *                          Edad: "18",
 *                          otro: "..."
 *                      }
 * @return {jquery object}  Retorna un TR con todos las las tablas TD
 */
function generaTableConentTr (obj) {
    var rer = $("<tr>");

    for ( key in obj ) {
            
        if (key == "id") {
            rer.append(
                $("<td>")
                    .attr({
                        class: key
                    })
                    .text(obj[key])
            )
        } else {
            var td = $("<td>")
                    .attr({
                        class: "editable",
                        "data-campo": key
                    })
                    .append(
                        $("<span>")
                        .text(obj[key])
                    )

            rer.append(
               td
            )
        }
    }

    return rer;
}