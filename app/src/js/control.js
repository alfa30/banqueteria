//////////////////////
// Deslogear cuenta //
//////////////////////
hash("OutLogin",function(){
	request({
		unlogin: "sessionControl"
	},function(res){
		window.location.href = "./";
	});
});

/////////////////////////
// Control de usuarios //
/////////////////////////
request({
	"recursive": true,
	"method":"REQUEST"
},{},function(res) {
	if (res.msj) {
		for (key in res.msj) {
			window.ale.new(res.msj[key].text,res.msj[key].type);
		};
	};
});

//////////////////////////////////////////////////////////////////////////////
// Verifica si la opción 'statusAccount' es falsa o no existe la aplicación //
// reirecciónar asta la pagina principal.                                   //
//                                                                          //
// Evita usar la aplicación si no tiene privilegios para usarla.            //
//////////////////////////////////////////////////////////////////////////////
request({
	statusAccount: "control"
},function(res){
	if (!res.statusAccount) {
		document.location.href = "index.html";
	};
});

/////////////////////////
// Control formato rut //
/////////////////////////
$("[type='rut']").each(function(index){
	$(this).rut({formatOn: 'keyup', validateOn: 'keyup'});
});

///////////////////////////////////////
// cargar listado de clientes #users //
///////////////////////////////////////
hash("users",function(){
	load.on();

	$("#users-tables-list-contact").find("tbody").html("");
	$("#user-table-control-list").hide();

	request({"getDataCliente": 0},function(res){

		for (var key in res.data) {
			// crea objetos
			var tr       = $("<tr>");
			var nn       = $("<td>");
			var nname    = $("<td>");
			var nrut     = $("<td>");
			var naddress = $("<td>");
			var ncontrol = $("<td>");

			//rellena objetos con sus respectivos valores
			nn.text(res.data[key].ID_CLIENTE);
			nrut.text(res.data[key].RUT);
			nname.text(res.data[key].NOMBRE+" "+res.data[key].APELLIDO);
			naddress.text(res.data[key].DIRECCION);

			// Crea objeto TR
			tr.append(nn);
			tr.append(nname);
			tr.append(nrut);
			tr.append(naddress);

			//agrega la nueva fila a la tabla
			$("#users-tables-list-contact").find("tbody").append(tr);
		}
		load.off();// apaga el proceso de carga 
	});
});

//////////////////////////////////////////////////////////
// Genera evento para el evento buscar por nombre o rut //
//////////////////////////////////////////////////////////
$("#users-form-buscar").submit(function(){

	load.on();
	$("#users-tables-list-contact").find("tbody").html("");
	$("#user-table-control-list").hide();


	// realiza la busqueda la servidor
	request({
		"getDataCliente"	: 0,
		"VALUE_NAME"		: this.name.value,
		"VALUE_RUT"			: this.rut.value
	},function(res){

		for (var key in res.data) {
			// crea objetos
			var tr       = $("<tr>");
			var nn       = $("<td>");
			var nname    = $("<td>");
			var nrut     = $("<td>");
			var naddress = $("<td>");
			var ncontrol = $("<td>");

			//rellena objetos con sus respectivos valores
			nn.text(res.data[key].ID_CLIENTE);
			nrut.text(res.data[key].RUT);
			nname.text(res.data[key].NOMBRE+" "+res.data[key].APELLIDO);
			naddress.text(res.data[key].DIRECCION);

			// Crea objeto TR
			tr.append(nn);
			tr.append(nname);
			tr.append(nrut);
			tr.append(naddress);

			//agrega la nueva fila a la tabla
			$("#users-tables-list-contact").find("tbody").append(tr);
		}

		load.off();// apaga el proceso de carga 
	})
	return false;
});

/////////////////////////////////////////////////////////////////
// Evento al momento de enviar la creación de una nueva cuenta //
/////////////////////////////////////////////////////////////////
$("#newClienteNewUsers").submit(function(){
	request({
		"method" : "POST"
	},{
		"newUsersCreate"		: true,
		"rut"					: this.rut.value,
		"email"					: this.email.value,
		"pass"					: this.password.value,
		"address"				: this.address.value,
		"name"					: this.name.value,
		"lastname"				: this.lastname.value,
		"phone"					: this.phone.value
	},function(res){
		console.log(res);
	});

	return false;
});

//////////////////////////////////////////////////
// Evento al momento de crear un nuevo articulo //
//////////////////////////////////////////////////
$("#nuevoArticuloSubirNuevoArticulo").submit(function() {
	request({
		"method": "POST"
	},{
		"nuevoArticuloSubirNuevoArticulo": true,
		"count": this.count.value,
		"category": this.category.value,
		"urlimage": this.urlimage.value,
		"name": this.name.value
	},function(res){
	});
	return false;
});

///////////////////////////////////////////////
// Evento para el formulario editar articulo //
///////////////////////////////////////////////
$("#aditarArticuloSubirArticulo").submit(function() {
	request({
		"method": "POST"
	},{
		"aditarArticuloSubirArticulo": true,
		"count": this.count.value,
		"category": this.category.value,
		"urlimage": this.urlimage.value,
		"name": this.name.value,
		"valueArticle": this.valueArticle.value
	},function(res){
	});
	return false;
});

//////////////////////////////////////////
// evento al cargar la ventana sesiones //
//////////////////////////////////////////
hash("sessions",function(){
	request({
		"selectSessionCollect": true
	},function(res){
		console.log(res);

		//recorre resultados
		for (var i in res.data) {
			// crea tr
			var tr       = $("<tr>");
			// crea objetos td
			var nn       = $("<td>");
			var nuser    = $("<td>");
			var nfecha   = $("<td>");
			var nbrowser = $("<td>");

			// Rellena datos
			nn.text(res.data[i].ID);
			nuser.text(res.data[i].ID_CLIENTE);
			nbrowser.text(res.data[i].BROWSER);
			nfecha.text(res.data[i].TIME_CREATE);

			// adhiere al objeto tr los objetos td
			tr.append(nn);
			tr.append(nuser);
			tr.append(nfecha);
			tr.append(nbrowser);

			$("#sessionControlManager").find("tbody").append(tr);
		}
	})
});

/////////////////////////////////
// carga listado de inventario //
/////////////////////////////////
var findInventario = function(search,searchType){
	request({
		"getData": "inventario",
		"findName": search,
		"findType": searchType
	},function(res){
		var fullInventario = [];
		for (var i in res.inventario) {
			for (var e in res.inventario[i]) {
				var articulo = res.inventario[i][e];
				articulo.TYPE = i;
				fullInventario.push(articulo);
			}
		}
		var tble = $("#tableviewlistinventariocompleto").find("tbody");
		tble.html("");

		for (var key in fullInventario) {

			var tr         = $("<tr>");
			var tdid       = $("<td>");
			var tdarticulo = $("<td>");
			var tdtipo     = $("<td>");
			var tdcantidad = $("<td>");
			var tdccontrol = $("<td>");

			tdid.text(parseInt(key)+1);
			tdarticulo.text(fullInventario[key].DESCRIP);
			tdtipo.text(fullInventario[key].TYPE);
			tdcantidad.text(fullInventario[key].STOCK);

			////////////////////////////////////////////
			// Agrega el botón Eliminar a la tabla    //
			////////////////////////////////////////////
			tdccontrol.append($("<button>").attr({
				"data-control": $.param(fullInventario[key])
			}).text("Eliminar").click(function() {
				var localVariable = $.parseParams($(this).attr("data-control"));

				////////////////////////////////////////////////////////////////
				// Enviar la instrucción para eliminar el objeto definido     //
				////////////////////////////////////////////////////////////////
				request({
					method: "POST"
				},{
					eliminarArticuloSubirArticulo : true,
					valueArticle                  : localVariable.ID,
					category                      : localVariable.TYPE
				},findInventario);

				return false;
			}));

			//////////////////////////////////////////
			// Agrega el botón Editar a la tabla    //
			//////////////////////////////////////////
			tdccontrol.append($("<button>").attr({
				"data-control": $.param(fullInventario[key])
			}).text("Editar").click(function() {
				var localVariable = $.parseParams($(this).attr("data-control"));
				console.log(localVariable);

				var form = $("#aditarArticuloSubirArticulo");
				form.find("[name='name']").val(localVariable.DESCRIP);
				seleccionarSelect(form.find("[name='category']"),localVariable.TYPE);
				form.find("[name='urlimage']").val(localVariable.IMAGEN);
				form.find("[name='count']").val(localVariable.STOCK);
				//valueArticle
				form.find("[name='valueArticle']").val(localVariable.ID);

				location.hash = "inventario-edit";
				return false;
			}));

			tr.append(tdid);
			tr.append(tdarticulo);
			tr.append(tdtipo);
			tr.append(tdcantidad);
			tr.append(tdccontrol);
			tble.append(tr);
		}
	});
};
hash('inventario',findInventario(null,null));

////////////////////////////
// Formulario de búsqueda //
////////////////////////////
$("#inventarioFiltarMedianteBusqueda").submit(function() {
	findInventario(this.name.value,this.type.value);
	return false;
});