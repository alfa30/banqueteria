/**
 * Permite seleccionar un elemento de un objeto select
 * 
 * @param  {Object} tuSelect Elemento o Elementos <select>
 * @param  {String} elemento Nombre del valor que se va a buscar
 * @return {Object}          Retorna todos los valores enviados en el parámetro tuSelect
 */
function seleccionarSelect(tuSelect,elemento) {
	if (tuSelect.length > 1) {
		for (var i in tuSelect) {
			seleccionarSelect(tuSelect[i],elemento);
		}
	} else {
		var combo = tuSelect;
		var cantidad = combo.length;
		for (i = 0; i < cantidad; i++) {
			if (combo[i].value == elemento) {
				combo[i].selected = true;
			}   
		}
	}
	return tuSelect;
}
