Menú
----

Como te había dicho podríamos poner algo parecido a esto en el inicio
mostrando todo lo que tenemos y cuando le toque la cliente ponemos lo
que le toque a cada paquete. Y para la creación del listado izquierdo
ocupamos el tipo\_menu de la tabla plato\_menu.

Te mando la URL para que veas mejor la página:
  ~ <http://www.florence.com.sv/menus.html>

La página diría algo así.

> “Tenemos una gran selección de platos para que usted y sus invitados
> tengan la mejor experiencia culinaria”

![Vista ejemplo de la pagina "Menú"](./files/QfDbA6.png)

Eventos
-------

Acá puedes poner las imágenes de algunas mesas y que se puedan ampliar
después.

Es página dirá.

> “Te mostramos algunas de nuestras decoraciones”

Nosotros:
---------

Es página dirá

> “¿Quiénes somos?, somos una empresa de Banquetearía que se ajusta a tu
> bolsillo, ofrecemos diferentes paquetes los cuales están amoldados A
> TI. Nosotros estamos orientados a la decoración de tu meza y presentar
> un exquisito menú orientado a un pequeño o mediano evento.

> Nuestra misión es brindar la mejor experiencia para nuestros clientes
> como sus invitados ofreciendo una gran variedad de servicios, con los
> cuales se sentirá a gusto.

> Nuestra visión será aumentar nuestros servicios a otro tipo de eventos
> y agregar más variedad de productos, todo con la misma calidad y
> dedicación que nosotros ofrecemos.”

Contacto
--------

Acá puedes pegar el formulario del Scott se llama `taller.cliente`

Es página dirá.

> “Si tienes dudas o contactarnos deja tus datos aquí nosotros te
> enviaremos más información.”

![Formulario de contato. Diseñado por Scott.](./files/hpoZS0.png)
