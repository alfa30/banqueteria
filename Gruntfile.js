module.exports = function (grunt) {
	grunt.initConfig({
		connect: {
			server: {
				options: {
					port: 9000,
					keepalive: true,
					host: 'localhost',
					open: true,
					base: "app/dist/"
				}
			}
		},
		less: {
			development: {
				options: {
					compress: true,
					yuicompress: true,
					optimization: 2
				},
				files: {
					// target.css file: source.less file
					"app/dist/themes/default/css/theme.delisius.css": [
						"app/src/less/theme.less"
					]
				}
			}
		},
		jade: {
			control: {
				files: {
					"app/dist/themes/default/index.html" : "app/src/jade/home.jade",
					"app/dist/themes/default/menus.html" : "app/src/jade/menu.jade",
					"app/dist/themes/default/eventos.html" : "app/src/jade/eventos.jade",
					"app/dist/themes/default/nosotros.html": "app/src/jade/nosotros.jade",
					"app/dist/themes/default/contacto.html": "app/src/jade/contacto.jade",
					"app/dist/themes/default/privacy-policy.html" : "app/src/jade/privacy-policy.jade",
					"app/dist/themes/default/terms-and-conditions.html": "app/src/jade/terms-and-conditions.jade"
				}
			},
			admin_control: {
				files: {
					"app/dist/themes/default/control.html" : "app/src/jade/admin/control.jade"
				}
			}, 
			admin_panel: {
				files: {
					"app/dist/themes/default/panel.html" : "app/src/jade/admin/panel.jade"
				}
			}
			// ,index: {
			// 	files: {
			// 		"app/dist/themes/default/index.html": "app/src/jade/home.jade"
			// 	}
			// }
		},
		uglify: {
			minify: {
			    options: {
			      mangle: {
			        except: ['jQuery', 'Backbone']
			      }
			    },
				files: {
					"app/dist/themes/default/js/general-control.js": [
						"app/dist/themes/default/js/jquery.min.js",
						"app/dist/themes/default/js/bootstrap.js",
						"app/dist/JRequest/request.js",
						"app/dist/themes/default/js/jquery.rut.js",
						"app/dist/themes/default/js/control-machine.js",
						"app/dist/themes/default/js/navegator-hash.js",
						"app/src/js/capJS-uploadfile.js",
						"app/src/js/Select Select.js",
						"app/src/js/parseParams.jquery.js"
					],
					"app/dist/themes/default/js/control.js": [
						"app/src/js/control.js",
						"app/src/js/pipe/pipe.js",
						"app/src/js/pipe/cliente.js",
						"app/src/js/pipe/contrato.js",
						"app/src/js/pipe/empleados.js",
						"app/src/js/pipe/pedidos.js"
					]
				}
			}
		},
		watch: {
			styles: {
				files: ['app/src/css/**/*.less'],
				tasks: ['less']
			},
			scripts: {
				files: ["app/src/js/**/*.js"],
				tasks: ["uglify"]
			},
			views: {
				files: ["app/src/jade/**/*.jade"],
				tasks: ["jade"]
			},
			views_admin: {
				files: ["app/src/jade/admin/**/*.jade"],
				tasks: ["jade:admin_control","jade:admin_panel"]
			}
		}
	});

	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-jade');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-copy');

	grunt.registerTask('default', ['dist']);
	grunt.registerTask('dist', ['less','uglify','jade']);
}